//
//  homeView.swift
//  myForecast
//
//  Created by Francisco Perez on 27/08/23.
//

import SwiftUI

struct homeView: View {
    @StateObject var locationManager = LocationManager()

    var body: some View {

        VStack {
            
            
            Text("Welcome")
                .accessibilityIdentifier("txt_welcome")
            
            if let location = locationManager.location {
                ContentView(Latitude: location.latitude, Longitude: location.longitude,citi:"")
                    //.environmentObject(locationManager)
            } else {
                if locationManager.isLoading {
                    loadingView()
                } else {
                    welcomeView()
                        .environmentObject(locationManager)
                }
            }
        }
        .background(Color(hue: 0.656, saturation: 0.787, brightness: 0.354))
        .preferredColorScheme(.light)
 
   }
}

struct homeView_Previews: PreviewProvider {
    static var previews: some View {
        homeView()
    }
}
