//
//  welcomeView.swift
//  myForecast
//
//  Created by Francisco Perez on 27/08/23.
//

import SwiftUI
import CoreLocationUI

struct welcomeView: View,GeneralProtocol {
    
    @EnvironmentObject var locationManager: LocationManager

    
        var body: some View {
            VStack {
                VStack(spacing: 20) {
                    Text("Welcome to your weather App")
                        .font(.title)
                        .foregroundColor(.white)
                        .accessibilityIdentifier("Text_Head")
                    
                    Text("Please share your current location")
                        .font(.title)
                        .foregroundColor(.white)
                        .padding()
                }
                .multilineTextAlignment(.center)
                .padding()
                
                
                
                // LocationButton from CoreLocationUI framework imported above, allows us to requestionLocation
                LocationButton(.shareMyCurrentLocation) {
                    locationManager.requestLocation()
                }
                .cornerRadius(30)
                .symbolVariant(.fill)
                .foregroundColor(.white)
                .accessibilityIdentifier("btn_location")
  
                Text(getInfo())
                    .font(.system(size:10,weight: .light))
                    .foregroundColor(Color.white)
                
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
        }
}

struct welcomeView_Previews: PreviewProvider {
    static var previews: some View {
        welcomeView()
    }
}

