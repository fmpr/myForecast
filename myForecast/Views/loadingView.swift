//
//  loadingView.swift
//  myForecast
//
//  Created by Francisco Perez on 27/08/23.
//

import SwiftUI

struct loadingView: View {
    var body: some View {
        ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: .white))
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        loadingView()
    }
}
