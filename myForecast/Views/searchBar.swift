//
//  searchBar.swift
//  myForecast
//
//  Created by Francisco Perez on 27/08/23.
//

import SwiftUI


struct searchBar: View {

    @StateObject var vm = HomeVM(endPoint: "")
    
    @Environment(\.presentationMode) var presentationMode
    @State private var showingModalView = false
    @State private var listCities = citiesList
    @State  var searchText:String = ""
    
    var body: some View {
        NavigationView {
            
                ScrollView {
                    ForEach(cities, id: \.self) { citi in
                        HStack {
                            Button(action: {
                                    self.showingModalView = true
                                    searchText = citi.lowercased()
                                
                                  }) {
                                    Text(citi.lowercased())
                                  }
                            
                                .onTapGesture {
                                    
                                    //print(citi.lowercased())
                                    
                                    Task {
                                        print(citi.lowercased())
                                        /*do {
                                            try await vm.loadInfobyCity(citi.lowercased())
                                            
                                            
                                        } catch { throw error }
                                         */
                                    }
                                    //self.presentationMode.wrappedValue.dismiss()
                                }
                            
                            Spacer()
                            
                            Image(systemName: "cloud.sun.fill")
                                .renderingMode(.original)
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width:40, height:40)
                            
                      
                   
                        }
                        .padding()
                        
                    }
                    
                }
                .searchable(text: $searchText)
                .sheet(isPresented: $showingModalView, content: {
                        ContentView(Latitude: 0, Longitude: 0,citi: searchText)
                })
                
                
            
        }
        
    }
    
    
    var cities:[String] {
        let lcCities = listCities.map { $0.lowercased()}
        if searchText.isEmpty {
                   return lcCities
               } else {
                   return lcCities.filter { $0.contains(searchText.lowercased()) }
               }
    }
  
}

struct searchBar_Previews: PreviewProvider {
    static var previews: some View {
        searchBar()
    }
}
