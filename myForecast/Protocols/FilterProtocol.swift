//
//  FilterProtocol.swift
//  myForecast
//
//  Created by Francisco Perez on 28/08/23.
//

import Foundation


protocol FilterProtocol {
    func filterInfo(lista: [List]) -> [List]
}
