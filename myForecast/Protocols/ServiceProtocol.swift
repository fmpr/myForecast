//
//  File.swift
//  myForecast
//
//  Created by Francisco Perez on 28/08/23.
//

import Foundation
import CoreLocation

protocol ServiceProtocol {
    var endPoint:String { get set }
    func loadInfo(latitude: CLLocationDegrees, longitude: CLLocationDegrees,citi:String) async throws -> ()
}
