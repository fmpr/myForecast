//
//  Service.swift
//  myForecast
//
//  Created by Francisco Perez on 26/08/23.
//

import Foundation


class Service:ObservableObject {
    
    @Published var forecast:[Forecast] = []
    
    static let shared = Service()
    private init() {
    }
    
    
    func getInfo<T:Decodable>(for: T.Type) async throws -> T{
        let endPoint:String = "https://api.openweathermap.org/data/2.5/forecast?q=London&appid=c580227ebd574b83338b64a04beebb7c"
        
        guard let url = URL(string: endPoint) else {
            throw errors_Enum.invalidUrl
        }
        
        let (data,response) = try await URLSession.shared.data(from: url)
        
        guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
            throw errors_Enum.responseError
        }
        
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
             
            return try await decoder.decode(T.self, from: data)
            
        } catch {
            throw errors_Enum.decodeErrorEndpoint
        }
    }
    
    func loadInfo() async throws {
        let endPoint:String = "https://api.openweathermap.org/data/2.5/forecast?q=London&appid=c580227ebd574b83338b64a04beebb7c"
        
        guard let url = URL(string: endPoint) else {
            throw errors_Enum.invalidUrl
        }
        
        let (data,response) =  try await URLSession.shared.data(from: url)
        
        guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
            throw errors_Enum.responseError
        }
        
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            
            let decoded =  try await decoder.decode([Forecast].self, from: data)
            
            
                self.forecast = decoded
            

            
        } catch {
            throw errors_Enum.decodeErrorEndpoint
        }
    }
    
}
