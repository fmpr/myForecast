//
//  errors_Enum.swift
//  myForecast
//
//  Created by Francisco Perez on 26/08/23.
//

import Foundation


enum errors_Enum: Error {
    case invalidUrl
    case responseError
    case decodeErrorEndpoint
    case loadInfoError
    case decodedResponseError
    case HomeVMTaskError
}
