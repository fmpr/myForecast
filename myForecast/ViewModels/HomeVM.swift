//
//  HomeViewModel.swift
//  myForecast
//
//  Created by Francisco Perez on 26/08/23.
//

import Foundation
import SwiftUI
import CoreLocation

class HomeVM: ObservableObject,FilterProtocol,ServiceProtocol {
    var endPoint: String
    @Published var forecasts = Forecast()

    init(endPoint:String) {
        self.endPoint = endPoint
    }
    
    // inherited from ServiceProtocol.loadInfo(latitude:longitude:citi:).
    func loadInfo(latitude: CLLocationDegrees, longitude: CLLocationDegrees, citi: String) async throws {
        
        if citi.isEmpty {
            endPoint = "https://api.openweathermap.org/data/2.5/forecast?lat=\(latitude)&lon=\(longitude)&appid=c580227ebd574b83338b64a04beebb7c"
        }
        else
        {
            endPoint = "https://api.openweathermap.org/data/2.5/forecast?q=\(citi)&appid=c580227ebd574b83338b64a04beebb7c"
            
        }
        
        guard let url = URL(string: endPoint) else {  throw errors_Enum.invalidUrl }
        
        let (data,response) =  try await URLSession.shared.data(from: url)
        
        guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {  throw errors_Enum.responseError  }
        
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            
            if let decodedresponse = try? decoder.decode(Forecast.self, from: data) {
        
                DispatchQueue.main.async {
                    self.forecasts = decodedresponse
                }
                
            } else  { throw errors_Enum.decodedResponseError }
        } catch { throw errors_Enum.loadInfoError  }
        
    }
    
    func filterInfo(lista: [List]) -> [List] {
            return lista.filter { item in
                let value1 = item.dtTxt ?? "NA"
                return value1.contains("15:00:00")
            }
    }
    
    
    func loadInfobyCity(_ citi:String) async throws {
        
        let endPoint:String = "https://api.openweathermap.org/data/2.5/forecast?q=\(citi)&appid=c580227ebd574b83338b64a04beebb7c"
        
        guard let url = URL(string: endPoint) else {  throw errors_Enum.invalidUrl }
        
        let (data,response) =  try await  URLSession.shared.data(from: url)
        
        guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {  throw errors_Enum.responseError  }
        
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            
            if let decodedresponse = try? decoder.decode(Forecast.self, from: data) {
        
                DispatchQueue.main.async {
                    self.forecasts = decodedresponse
                }
                
            } else  { throw errors_Enum.decodedResponseError }
        } catch { throw errors_Enum.loadInfoError  }
        
        
    }
    
}
