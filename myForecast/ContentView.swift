//
//  ContentView.swift
//  myForecast
//
//  Created by Francisco Perez on 26/08/23.
//

import SwiftUI
import CoreLocation

func getTemp(value:Int) -> Int {
    return Int(Double((value * 9/5)) - 459.67)
}

struct ContentView: View, GeneralProtocol {
    
    //@EnvironmentObject var locationManager: LocationManager
    @StateObject var vm = HomeVM(endPoint: "")
   
    
    @State var lista:[List] = []
    @State var listado:[List] = []
    @State private var isNight = true
    @State private var showingModalView = false
    var Latitude: CLLocationDegrees
    var Longitude: CLLocationDegrees
    var citi:String 

    
    
    var body: some View {
        

            NavigationView {
                VStack {
                    
                    Text(vm.forecasts.city?.name ?? "My city")
                        .font(.system(size:50,weight: .medium))
                        .foregroundColor(Color.blue)
                    
                    var icon:String = vm.forecasts.list?.first?.weather?.first?.icon ?? "04d"
                    AsyncImage(url: URL(string: "https://openweathermap.org/img/wn/\(icon)@2x.png"))
                        .frame(width:40, height:40)
                        .padding()
                    Text(vm.forecasts.list?.first?.weather?.first?.description ?? "Description")
                        .font(.system(size:25,weight: .medium))
                        .foregroundColor(Color.white)
                    
                    
                    Text("\(getTemp(value: Int(vm.forecasts.list?.first?.main?.temp ?? 0))) °F")
                        .font(.system(size:70,weight: .medium))
                        .foregroundColor(Color.white)
                    
                    HStack {
                        CityTextView(lista: vm.filterInfo(lista: vm.forecasts.list ?? listado))
                    }
                    
                    NavigationLink(destination: searchBar()) {
                        Text("Check another location")
                    }
                    
                    Button{
                        self.showingModalView = true
                    } label:{
                        Label("Share",systemImage: "heart.fill")
                    }
                    .buttonStyle(.bordered)
                    .tint(.green)
                    .controlSize(.large)
                    .padding()
                    
                    Text(getInfo())
                        .font(.system(size:10,weight: .light))
                        .foregroundColor(Color.blue)
                }
                .background(
                    LinearGradient(gradient: Gradient(colors: [.white, .blue ,.white]), startPoint: .top, endPoint: .bottom)
                    
                )
                .padding()
                
                
                .onAppear {
                    Task {
                        do {
                            
                            try await vm.loadInfo(latitude: Latitude,longitude:Longitude,citi:citi)
                            //lista = vm.filterInfo(lista: vm.forecasts.list ?? listado)
                            //print(lista)
                        } catch { throw errors_Enum.HomeVMTaskError }
                    }
                }
                
            } // navigation view
            .sheet(isPresented: $showingModalView, content: {
                //searchBar()
                
            })
            
        
    }
        
        
}




struct CityTextView: View {
    
    var lista: [List]
    var body: some View{
        
        
            ForEach(lista) { list in
                
                let icono = list.weather?.first?.icon?.description ?? "04d"
                VStack {
                    AsyncImage(url: URL(string: "https://openweathermap.org/img/wn/\(icono)@2x.png"))
                        .frame(width:7, height:7)
                        .padding()
                    Text(list.weather?.first?.main ?? "")
                        .font(.system(size:10,weight: .semibold,design: .monospaced))
                        .foregroundColor(Color.white)

                    Text("\(getTemp(value:Int(list.main?.temp ?? 0))) °F")
                        .foregroundColor(Color.white)

                    Text(getdate_(timestampInSeconds: list.dt ?? 0000001))
                        .font(.system(size:11,weight: .ultraLight,design: .monospaced))
                        .foregroundColor(Color.black)
                        .padding()
                    
                    
                    /*
                    Text(list.dtTxt?.description ?? "NA")
                        .font(.system(size:20,weight: .light,design: .default))
                        .foregroundColor(Color.black)
                        .padding()
                     */
                     
            }
        }
    }
    

    
    func getdate_(timestampInSeconds:Int) -> String {

        let dateFromTimestampInSeconds = Date(timeIntervalSince1970: TimeInterval(timestampInSeconds))
        
        return dateFromTimestampInSeconds.formatted(date: .abbreviated, time: .omitted)
    }
        
} // end struct CityTextView




struct ContentView_Previews: PreviewProvider {
    
    // mock data
    static var previews: some View {
        ContentView(Latitude: 0, Longitude: 0,citi:"")
    }
}
