Hello welcome to myForecast App 


This app was built with SwiftUI iOS 16.4

The restAPI that i used was: http://api.openweathermap.org/data/2.5/weather?q=London,uk&APPID=c580227ebd574b83338b64a04beebb7c

I created a free key and used  the Call 5 day / 3 hour forecast data, this end point displayed 3 hour forecast data so i created a function to filter the information to display a 5 day 24 hr

I used MVVM pattern because its well known pattern used in mobile application, its scalable and facilitates the separation of the development of the graphical user interface, parsing the information into my models,connecting to my view model and display on my Views

I used Protocols for inheritance and concurrency with async and await

I wanted to use Combine for reactive programming,creating a generic network layer and UI Test but i ran out time

I wanted to add a save locations for a better experience

How to use the app:

the app was created and tested in iOS 16.4
When the app is launched we need to give our current location tapping the Share my Current location button

we can search for a new locations just tapping the Check another location button, its displays a list of cities,just select one city and it will display the information.

by Francisco Martin P.


